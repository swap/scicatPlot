# scicatPlot

[![Build Status](https://travis-ci.org/garethcmurphy/scicatPlot.svg?branch=master)](https://travis-ci.org/garethcmurphy/scicatPlot)

- Query data catalogue for data files by name
- Create plots automatically
- Upload as attachments to catalogue
- 
- Delete origdatablocks and replace with updated origdatablocks with nee file location info
- Updates metadata from file by deleting dataset and replacing

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
h5py
matplotlib
requests
```

### Installing

How to install

```
pipenv install
```

```
pipenv run start
```

## Running the tests

Explain how to run the automated tests for this system

```
pytest
```

### And coding style tests

Explain what these tests test and why

```
pylint
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

- [Pipenv](https://github.com/pypa/pipenv) - Python development framework

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

- **Gareth Murphy** - _Initial work_ - [garethcmurphy](https://github.com/garethcmurphy)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

- Hat tip to anyone whose code was used
- Inspiration
- etc
- 

## Functions

- data_dir - list files
- file_info - get file info in scicat format
- get_api - gets the api address
- scicat_attach - attaches a file with base64 encoding
- scicat_login - logs into scicat
- scicat_met - loop around files and update metadata
- scicat_orig - loop around files and update orig_datablocks
- scicat_reconcile - check if files on disk are reflected in scicat (incomplete)
- scicat_search - search scicat for plain text
- scicat_vis - loop around files and add visualisation


